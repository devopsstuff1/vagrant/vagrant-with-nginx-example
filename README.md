# About
This is simple vagrant box with **Ansible** provisioner. This will run simple **Nginx** server with port forwarding *from 80(guest) to 8081(host)*

# Usage
You must have **Vagrant** and **Ansible** preinstalled.
Change current working directory to directory with Vagrantfile using **cd** and run
```console
vagrant up
```
